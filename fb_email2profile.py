import urllib2
import json
import argparse
import urllib
import os
import webbrowser
from pprint import pprint
import Queue
import threading

i = 0

def lookupAccount(email, access_token, browser_open):
	try:
		queryStr = "https://graph.facebook.com/search?q=%s&type=user&access_token=%s" % (email, access_token)
		jsonData = urllib2.urlopen(queryStr)
	except urllib2.HTTPError, e:
		if e.code == 400:
			print 'We need a new access token!'
			exit()

	if jsonData:
		data = json.load(jsonData)
		if len(data['data']) == 1:
			if len(data['data'][0]) == 2:
				try:
					iFacebookID		= data['data'][0]['id']
					szFacebookName	= data['data'][0]['name']
					
					queryExtStr = "https://graph.facebook.com/%s" % (iFacebookID)
					jsonExtextData = urllib2.urlopen(queryExtStr)
	
					if jsonExtextData:
							extData = json.load(jsonExtextData)

							return extData
				except:
					print 'Unexpected is always expected!'
	return ()


def Worker (q, l, access_token, open_browser):
	global i
	while True:
		try:
			q_item = q.get_nowait()
			if q_item:
				 accountData = lookupAccount(q_item['email'], access_token, 0)
				 if accountData:
					 print "[%d] %s:%s" % (i, accountData['link'].encode('ascii', 'ignore'),  q_item['email'].encode('ascii', 'ignore'))
					 if open_browser:
						 webbrowser.open(accountData['link'], new=2)
				 l.acquire()
				 i = i + 1
				 l.release()

		except Queue.Empty:
			print 'Processed all of the items!'
			return 0


q = Queue.Queue()

fbAccessToken = ""
optParser = argparse.ArgumentParser(description='Checks whether facebook account exists for given email')
optParser.add_argument('-email', action="store", dest="email", required=True, help = 'Email address or file containing email addresses')
optParser.add_argument('-open', action="store_true", dest="open_browser", required=False, help = 'Open facebook profile in browser when account is found')
optParser.add_argument('-limit', action="store", dest="processing_limit", required=False, help = 'Limits a number of processed entries by given number')
optParser.add_argument('-threads', action="store", dest="num_threads", required=False, default = 4, help = 'Number of worker threads')
optParser.add_argument('-token', action="store_true", dest="get_token", required=False, default = False, help = 'Opens a facebook graph api page for oauth token')


arguments = optParser.parse_args()

if arguments.get_token:
	webbrowser.open('https://developers.facebook.com/docs/reference/api/selecting-results/', new = 2)
	exit()


num_counter = 1

if os.path.exists(arguments.email):
	bSingle = False
else:
	bSingle = True

if bSingle:
	lookupAccount(arguments.email, fbAccessToken)
else:
	fh = open(arguments.email, 'r')

	for line in fh:
		entryStrip = line.strip()
		urllib.quote(entryStrip)

		if arguments.processing_limit and num_counter > int(arguments.processing_limit):
			break

		worker_data = {'email' : entryStrip}
		q.put(worker_data)

	fh.close()

l = threading.Lock()

for i in xrange(0, int(arguments.num_threads)):
	t = threading.Thread(target=Worker, args=(q, l, fbAccessToken, arguments.open_browser))
	t.start()